# Next.JS

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.


## MUI IN NEXT

En su sitio oficial [MUI](https://mui.com/), podemos ver mas para nuestro sitio.

### 1.	npx create-next-app **NombreDelproyecto**

### 2.	npm install @mui/material @emotion/react @emotion/styled @emotion/server @mui/icons-material

### 3.	Create a custom file `/pages/_document.js` and add the following code to it.

```bash
import * as React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';
import createEmotionServer from '@emotion/server/create-instance';
import theme from '../src/theme';
import createEmotionCache from '../src/createEmotionCache';
  
export default class MyDocument extends Document {
    render() {
        return (
            <Html lang="en">
                <Head>
                    {/* PWA primary color */}
                    <meta name="theme-color" 
                        content={theme.palette.primary.main} />
                    <link rel="shortcut icon" 
                        href="/static/favicon.ico" />
                    <link
                        rel="stylesheet"
                        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
                    />
					{/* Inject MUI styles first to match with the prepend: true configuration. */}
                    {this.props.emotionStyleTags}
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}
  
// \`getInitialProps\` belongs to \`_document\` (instead of \`_app\`), it\'s compatible with static-site generation (SSG).

MyDocument.getInitialProps = async (ctx) => {
     
    const originalRenderPage = ctx.renderPage;
  
    // You can consider sharing the same emotion cache between 
    // all the SSR requests to speed up performance.
    // However, be aware that it can have global side effects.
     
   const cache = createEmotionCache();
    const { extractCriticalToChunks } = createEmotionServer(cache);
  
    ctx.renderPage = () =>
        originalRenderPage({
            enhanceApp: (App) =>
                function EnhanceApp(props) {
                    return <App emotionCache={cache} {...props} />;
                },
        });
  
    const initialProps = await Document.getInitialProps(ctx);
  
    // This is important. It prevents emotion to render invalid HTML.
    // See 
	// https://github.com/mui-org/material-ui/issues/26561#issuecomment-855286153
      
    const emotionStyles = extractCriticalToChunks(initialProps.html);
    const emotionStyleTags = emotionStyles.styles.map((style) => (
        <style
            data-emotion={`${style.key} ${style.ids.join(' ')}`}
            key={style.key}
  
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{ __html: style.css }}
        />
    ));
  
    return {
        ...initialProps,
        emotionStyleTags,
    };
};
```

### 4.	Create an `src folder`, add `theme.js` and `createEmotionCache.js` files as below

```bash
import { createTheme } from '@mui/material/styles';
import { red } from '@mui/material/colors';
  
// Create a theme instance.
const theme = createTheme({
    palette: {
        primary: {
            main: '#556cd6',
        },
        secondary: {
            main: '#19857b',
        },
        error: {
            main: red.A400,
        },
    },
});
  
export default theme;

import createCache from '@emotion/cache';
  
export default function createEmotionCache() {
    return createCache({ key: 'css', prepend: true });
}
``` 

### 5.	Update the file `/pages/_app.js` with the below code.

```bash
import * as React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { CacheProvider } from '@emotion/react';
import theme from '../src/theme';
import createEmotionCache from '../src/createEmotionCache';
  
// Client-side cache shared for the whole session 
// of the user in the browser.
  
const clientSideEmotionCache = createEmotionCache();
  
export default function MyApp(props) {
    const { Component, emotionCache = 
        clientSideEmotionCache, pageProps } = props;
  
    return (
        <CacheProvider value={emotionCache}>
            <Head>
                <meta name="viewport" 
                    content="initial-scale=1, width=device-width" />
            </Head>
            <ThemeProvider theme={theme}>
                  
                {/* CssBaseline kickstart an elegant, 
                consistent, and simple baseline to
                build upon. */}
                  
                <CssBaseline />
                <Component {...pageProps} />
            </ThemeProvider>
        </CacheProvider>
    );
}
  
MyApp.propTypes = {
    Component: PropTypes.elementType.isRequired,
    emotionCache: PropTypes.object,
    pageProps: PropTypes.object.isRequired,
};
```

### 6.	Update the file `/pages/index.js` with the below code
```bash
import Head from "next/head";
import styles from "../styles/Home.module.css";
  
export default function Home() {
    return (
        <div className={styles.container}>
            <Head>
                <title>Create Next App</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
  
            <main className={styles.main}>
                <h1 className={styles.title}>
                    Welcome to <a href="https://nextjs.org">
                        Next.js!</a> integrated with{" "}
                    <a href="https://mui.com/">Material-UI!</a>
                </h1>
                <p className={styles.description}>
                    Get started by editing{" "}
                    <code className={styles.code}>
                        pages/index.js</code>
                </p>
  
            </main>
        </div>
    );
}
```